from rdflib import Graph, Namespace
from flask_table import Table, Col
import query

bkb 		= Namespace("https://budayakb.cs.ui.ac.id/ns#")
bkbr 		= Namespace("https://budayakb.cs.ui.ac.id/resource/")
db 			= Namespace("http://dbpedia.org/resource/")
dbo 		= Namespace("http://dbpedia.org/ontology/")
wdt			= Namespace("http://www.wikidata.org/prop/direct/")
rdfs		= Namespace("http://www.w3.org/2000/01/rdf-schema#")
foaf		= Namespace("http://xmlns.com/foaf/0.1/")
fifa		= Namespace("https://sofifa.com/player/")
p 			= Namespace("http://www.examplepath.org/")
wd  		= Namespace("http://www.wikidata.org/entity/")

def getPlayer(input):
	graph = query.getGraph()
	temp=""
	players = []
	for s,p,o in graph.triples( (None, None, db.SoccerPlayer) ):
	
		for a,b,c in graph.triples( (s, dbo.Name, None) ):
			if str(c) == input:			
				players.append(str(c))
				temp=a

	for x,y,z in graph.triples( (temp, dbo.birthDate, None) ):
		bd = str(z)
		birth = bd[0:9]
		players.append(birth)
	for x,y,z in graph.triples( (temp, dbo.height, None) ):
		players.append(str(z))
	for x,y,z in graph.triples( (temp, dbo.weight, None) ):
		players.append(str(z))
	for x,y,z in graph.triples( (temp, dbo.idNumber, None) ):
		players.append(str(z))
	
	return players

def getTeam(input):
	graph = query.getGraph2()
	temp = ""
	temp2 = ""
	team = []
	for s,p,o in graph.triples( (None, wdt.P118, None) ):
		for a,b,c in graph.triples( (s, rdfs.label, None) ):
			if str(c.encode('utf8')) == str(input):
				team.append(str(c.encode('utf8')))
				temp=a

	#league
	for x,y,z in graph.triples( (temp, wdt.P118, None) ):
		for a,b,c in graph.triples( (z, rdfs.label, None) ):
				team.append(str(c))
				temp2=a
	#country
	for x,y,z in graph.triples( (temp2, wdt.P17, None) ):
		team.append(str(z))
	team.append(str(temp))

	return team