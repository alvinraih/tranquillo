import showTable
from flask import Flask, request, Markup, render_template
from flask_restful import Resource, Api
import codecs

app = Flask(__name__, static_url_path='/static')
api = Api(app)

@app.route('/')
def start():
	return render_template("index.html")

@app.route('/data/<input>')
def data(input):
    table = cariPlayer(input)
    return render_template("display.html", table=table, input=str(input))

@app.route('/<input>')
def cariPlayer(input):
	hasilcari = showTable.getPlayer(input)
	return hasilcari

@app.route('/<input>')
def cariTeam(input):
    hasilcari = showTable.getTeam(input)
    return hasilcari

@app.route('/search')
def cari():
	return render_template("search.html")

@app.route('/search2')
def cari2():
    return render_template("search2.html")

@app.route('/hasil_data', methods=['POST'])
def hasil_data():
    input = request.form['input']
    table = cariPlayer(input)
    var = ['Name','Birthdate','Weight (lbs)','Height (cm)']
    return render_template("display.html", table=table, input=str(input), len=len(table), var=var)

@app.route('/hasil_data2', methods=['POST'])
def hasil_data2():
    input = request.form['input']
    table = cariTeam(input)
    var = ['Name','League','Country']
    return render_template("display2.html", table=table, input=str(input), len=len(table), var=var)

if __name__ =='__main__':
   app.run(debug=True)
