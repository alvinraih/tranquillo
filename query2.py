from rdflib import Graph
from SPARQLWrapper import SPARQLWrapper, RDF

def getGraph():
	g = Graph()
	g.parse("budaya.ttl", format="ttl")

	sparql1 = SPARQLWrapper("http://id.dbpedia.org/sparql")
	query1 = """
		PREFIX dbpedia: <http://id.dbpedia.org/resource/> 
		PREFIX bkbr: 	<https://budayakb.cs.ui.ac.id/resource/>
		PREFIX kat: 	<http://id.dbpedia.org/resource/Kategori:>
		PREFIX foaf:	<http://xmlns.com/foaf/0.1/>
		CONSTRUCT 	
		{
			?Player dbpedia-owl:abstract 	?Abstract.
			?Player dbpedia-owl:thumbnail	?Thumbnail.
			?Player foaf:isPrimaryTopicOf	?wiki
		}
		WHERE		
		{
			?Player dbpedia-owl:abstract 	?Abstract.
			?Player dbpedia-owl:thumbnail	?Thumbnail.
			?Player dcterms:subject		dbpedia:SoccerPlayer.
			?Player foaf:isPrimaryTopicOf	?wiki
		}
	"""
	sparql1.setQuery(query1)
	sparql1.setReturnFormat(RDF)
	results1 = sparql1.query()
	triples1 = results1.convert()
	g += triples1



	sparql = SPARQLWrapper("https://query.wikidata.org/sparql")
	query = """
		CONSTRUCT 
		{
			?player rdfs:label ?playerLabel.
			?player wdt:P361  ?wdt:Q476028.
			?wdt:Q476028 rdfs:label ?club
		}
		WHERE
		{
			?item wdt:P31   wd:Q2095.
			?item wdt:P17   wd:Q252.
			?item wdt:P186  ?material.
			?item rdfs:label ?item_label filter (lang(?item_label) = "id").
			?material rdfs:label ?material_label filter (lang(?material_label) = "id").
			SERVICE wikibase:label { bd:serviceParam wikibase:language "id,en" }
		}
	"""
	sparql.setQuery(query)
	sparql.setReturnFormat(RDF)
	results = sparql.query()
	triples = results.convert()
	g += triples
	
	return g